public class MoedeiroSimples{
  int umCentavo;
  int cincoCentavos;
  int dezCentavos;
  int cinquentaCentavos;
  int umReal;

  public void adicionarUmCentavo(){
    this.umCentavo++;
  }
  public void removerUmCentavo(){
    if(this.umCentavo > 0) this.umCentavo--;
  }
  public int contarUmCentavo(){
    return this.umCentavo;
  }
  public float calcularValorUmCentavo(){
    return this.umCentavo*0.01;
  }

  public void adicionarCincoCentavos(){
    this.cincoCentavos++;
  }
  public void removerCincoCentavos(){
    if(this.cincoCentavos > 0) this.cincoCentavos--;
  }
  public int contarCincoCentavos(){
    return this.cincoCentavos;
  }
  public float calcularValorCincoCentavos(){
    return this.cincoCentavos*0.05;
  }
}
