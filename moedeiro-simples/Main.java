/*
1) Descreva o que faz cada linha do metodo main da classe Main (olhe o que faz cada metodo do moedeiro na classe Moedeiro).
2) Faca com que o moedeiro tenho 20 moedas de um centavo.
3) Crie e use os metodos no moedeiro que sirvam para adcionar, remover, contar e calcular valor para moedas de 10 centavos.
4) Crie e use os metodos no moedeiro que sirvam para adcionar, remover, contar e calcular valor para moedas de 25 centavos.
5) Crie e use os metodos no moedeiro que sirvam para adcionar, remover, contar e calcular valor para moedas de 50 centavos.
6) Crie e use os metodos no moedeiro que sirvam para adcionar, remover, contar e calcular valor para moedas de 1 real.
7) Crie e use os metodo para contar e calcular valor de TODAS as moedas no moedeiro.
*/

public class Main{
  public static void main(String[] args){
    MoedeiroSimples moedeiroSimples = new MoedeiroSimples();
    for(int i =0;i<10;i++){
      moedeiroSimples.adicionarUmCentavo();
    }

    moedeiroSimples.adicionarCincoCentavos();
    moedeiroSimples.adicionarCincoCentavos();
    moedeiroSimples.adicionarCincoCentavos();
    moedeiroSimples.adicionarCincoCentavos();
    moedeiroSimples.adicionarCincoCentavos();

    int quantidadeDeMoedasDeUmCentavo = moedeiroSimples.contarUmCentavo();
    float valorDasMoedasDeUmCentavo = moedeiroSimples.calcularValorUmCentavo();

    int quantidadeDeMoedasDeCincoCentavos = moedeiroSimples.contarCincoCentavos();
    int valorDasMoedasDeCincoCentavos = moedeiroSimples.calcularValorCincoCentavos();

    System.out.println("qtde de moedas de 0.01:"+quantidadeDeMoedasDeUmCentavo);
    System.out.println("valor em moedas de 0.01:"+valorDasMoedasDeUmCentavo);

    System.out.println("qtde de moedas de 0.05:"+quantidadeDeMoedasDeCincoCentavos);
    System.out.println("valor em moedas de 0.05:"+valorDasMoedasDeCincoCentavos);
  }
}
