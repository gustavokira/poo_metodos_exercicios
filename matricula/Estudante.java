public class Estudante{
  String nome;
  String sobrenome;
  int anoDeIngresso;
  int semestreDeIngresso;
  int fase;
  Disciplina[] disciplinasAtuais;

  public Estudante(){
    this.disciplinasAtuais = new Disciplina[4];
  }

  public int contarDisciplinas(){
    int quantidade = 0;
    for(int i =0;i<this.disciplinasAtuais.length;i++){
      if(this.disciplinasAtuais[i] != null){
        quantidade++;
      }
    }
    return quantidade;
  }

  public boolean addDisciplina(Disciplina d){
    for(int i =0;i<this.disciplinasAtuais.length;i++){
      if(disciplinasAtuais[i] == null){
        this.disciplinasAtuais[i] = d;
        return true;
      }
    }

    return false;
  }


}
