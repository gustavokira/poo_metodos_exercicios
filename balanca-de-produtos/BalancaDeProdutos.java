public class BalancaDeProdutos{

  public Produto maisPesado(Produto p1, Produto p2){
    if(p1.peso > p2.peso) return p1;
    else if(p1.peso < p2.peso) return p2;

    return null;
  }
}
